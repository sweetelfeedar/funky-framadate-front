module.exports = {
	parser: '@typescript-eslint/parser',
	plugins: ['@typescript-eslint', 'json'],
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'plugin:prettier/recommended',
		'prettier/@typescript-eslint',
		'plugin:json/recommended',
	],
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		project: './tsconfig.json',
		tsconfigRootDir: __dirname,
	},
	rules: {
		'@typescript-eslint/unbound-method': ['error', { ignoreStatic: true }],
	},
};
