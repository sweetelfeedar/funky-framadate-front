import { Component, Input, OnInit } from '@angular/core';
import { StorageService } from '../../../core/services/storage.service';
import { Choice } from '../../../core/models/choice.model';
import { Answer } from '../../../core/enums/answer.enum';
import { Poll } from '../../../core/models/poll.model';

@Component({
	selector: 'app-choice-button',
	templateUrl: './choice-button.component.html',
	styleUrls: ['./choice-button.component.scss'],
})
export class ChoiceButtonComponent implements OnInit {
	@Input() public poll: Poll;
	@Input() public choice: Choice;
	@Input() public answerKind: string = 'yes';

	public answerEnum = Answer;

	constructor(public storageService: StorageService) {}

	ngOnInit(): void {}
}
