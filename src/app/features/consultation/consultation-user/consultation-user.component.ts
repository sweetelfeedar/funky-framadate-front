import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';
import { DateUtilitiesService } from '../../../core/services/date.utilities.service';
import { Router } from '@angular/router';
import { ToastService } from '../../../core/services/toast.service';
import { DOCUMENT } from '@angular/common';
import { StorageService } from '../../../core/services/storage.service';
import { ApiService } from '../../../core/services/api.service';

@Component({
	selector: 'app-consultation-user',
	templateUrl: './consultation-user.component.html',
	styleUrls: ['./consultation-user.component.scss'],
})
export class ConsultationUserComponent implements OnInit {
	pollName: string;
	private pass_hash: any;
	constructor(
		private dateUtilitiesService: DateUtilitiesService,
		private router: Router,
		private api: ApiService,
		private toastService: ToastService,
		private cd: ChangeDetectorRef,
		@Inject(DOCUMENT) private document: any,
		public storageService: StorageService,
		public pollService: PollService
	) {
		this.pollService.step_current = 2;
	}

	ngOnInit(): void {}

	sendVoteStack() {
		this.addVoteStack();
	}
	/**
	 * create a	new vote stack
	 */
	addVoteStack(): void {
		this.storageService.vote_stack.custom_url = this.pollService._poll.getValue().custom_url;
		this.pollService.pass_hash = this.pass_hash;

		this.toastService.display('envoi du vote ....');
		this.api
			.sendNewVoteStackOfPoll(this.storageService.vote_stack)
			.then((resp: any) => {
				console.log('sendNewVoteStackOfPoll resp', resp);
				this.storeVoteStackAndReloadPoll(resp);
				this.router.navigate(['/poll/' + this.pollService._poll.getValue().custom_url + '/consultation']);
				this.toastService.display('woot !');
			})
			// eslint-disable-next-line @typescript-eslint/unbound-method
			.catch(this.api.ousideHandleError);
	}
	/**
	 * store the updated vote stack
	 * @param voteStack
	 */
	storeVoteStackAndReloadPoll(voteStack: any) {
		if (voteStack.status == 200) {
			this.storageService.mapVotes(voteStack.data);
			this.pollService.enrichVoteStackWithCurrentPollChoicesDefaultVotes(this.storageService.vote_stack);
			if (this.pass_hash) {
				this.pollService.loadPollByCustomUrlWithPasswordHash(
					this.pollService._poll.getValue().custom_url,
					this.pass_hash
				);
			} else {
				this.pollService.loadPollByCustomUrl(this.pollService._poll.getValue().custom_url);
			}
		} else {
			this.toastService.display('erreur à l enregistrement');
		}
	}
}
