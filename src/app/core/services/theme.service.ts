import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Theme } from '../enums/theme.enum';

@Injectable({
	providedIn: 'root',
})
export class ThemeService {
	private _theme: BehaviorSubject<Theme> = new BehaviorSubject<Theme>(Theme.LIGHT);
	public readonly theme: Observable<Theme> = this._theme.asObservable();

	public selectTheme(theme: Theme): void {
		this._theme.next(theme);
	}
}
