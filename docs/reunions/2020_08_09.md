Framadate suivi - Avril 2020
==
###### tags: `framadate` `suivi`

> Participants à la réunion: Maiwann / Tykain / Côme / Seraf

## État des lieux

> Où on en est sur la version funky ?

https://framagit.org/framasoft/framadate/funky-framadate-front/-/boards

> Quel est le projet ? Quels sont les enjeux ?  
- Avoir un truc utilisable (sur téléphone !)
- Graphiquement plus élégant

> Quelles méthodes de travail ?  
- Une refonte ergonomique avant de faire des tests utilisateurs pour vérifier que la nouvelle interface est chouette 

> Quel niveau de qualité ? standards et de bonnes pratiques ?  
- Une refonte graphique, utilisable sur mobile
- De l'internationalisation
- Un logiciel accessible

> Quelles priorités ?  
- Cycle de vote à finir

> Qui veut faire quoi ?  


## Notes de réunion

> Le nom semble peu approprié depuis l'apparition des nouvelles fonctionnalités.

Suggestions :
- Framapool
- FramEnquête
- Framasondage

> Idée de nouvelle fonctionnalité (pour plus tard hein :D )

- Chaque utilisateur indique ses disponibilités et le service renvoie les plages pour lesquelles le plus de personnes sont disponibles.

## Trucs à faire

- Gitlab CI : exécuter les tests automatiquement
- envoi de mail au créateur du sondage
- cafouillage entre front & back quand édition d'un sondage (à préciser par tykain)
- vérifier l'accesibilité du formulaire
- prompt de modale pour accès par mot de passe a un sondage privé

## Remarques

Il est difficile d'entrer dans le projet en absence de spécifications écrites. (seraf)

## Décisions prises

- Vérifier que chacun.e arrive à faire tourner le projet
- Chacune choisit des issues / tickets adaptés à ce qu'il a envie de faire

## Ressources

* Discussion : https://framateam.org/ux-framatrucs/channels/framadate
* Repo front/dev : https://framagit.org/framasoft/framadate/funky-framadate-front/tree/dev 
* Repo back : https://framagit.org/tykayn/date-poll-api
* Maquettes Zeplin : https://scene.zeplin.io/project/5d4d83d68866d6522ff2ff10
* La démo : https://framadate-api.cipherbliss.com/
* Vidéo de présentation : https://nuage.maiwann.net/s/JRRHTR9D2akMAa7
